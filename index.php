<!DOCTYPE html>
<html lang="en">
<head>
    <title>RV Lots for Sale - Deeded RV Lot in Resort Community.</title>
    <META name="description" content="Mountain Resort RV Lots. RV Lots for sale in Georgia." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    <script src="sweetalert-master/dist/sweetalert-dev.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/main.js"></script>
</head>
<body>
    <header>
        <div class="head_name_wrap">
            <div class="head_name">
                <span>North Georgia RV Lots</span>
                <div class="head_menu_button fa fa-bars"></div>
            </div>
            <div class="head_menu_onclick">
                <a href="index.php">Home</a>
                <a href="property-listings.php">Property Listing</a>
                <a href="the-community.php">The Community</a>
                <a href="gallery.php">Gallery</a>
                <a href="contact-us.php">Contact Us</a>
            </div>
        </div>
        <?php include 'head.php' ; ?><!--****************HEAD****************-->
    </header>
    <main class="main">
        <div class="main_content">
            <div class="leftSideBar">
                <div class="content_header content_font">
                    <p class="content_headertext1">RV lots For Sale</p>
                    <p class="content_headertext2">Only 5 minutes from Carters Lake Marina. </p>
                    <p class="content_headertext3">1 hour - 15 minutes North of Atlanta, GA</p>
                </div>
                <div class="content_main">
                    <div class="content_main_textBar">
                        <img class="img_home" src="images/home_photo_2.jpg" alt=""/>
                        <span class="textBar_link">Talking Rock RV Resort</span>, located just west of Ellijay GA, offers the opportunity to own your very own <a href="property-listings.html">deeded RV lot</a>  in the beautiful mountain foothills of north Georgia. Only 5 minutes from 3,500 acre <a href="http://carters.sam.usace.army.mil/" target="_blank">Carters Lake </a> , considered by many to be Georgia's most spectacular and serene mountain lake. Well stocked with striped bass, hybrids and walleye, it is a fisherman's paradise.
                    </div>
                    <div class="content_main_textBar">
                        <img class="img_home2" src="images/home_photo_3.jpg" alt=""/>
                        With this comes a full amenity package including an all new clubhouse. Pool, tennis, pavilion, walking trails with excercise stations, excercise room, shower and laundry facilities, and modern playground equipment are just a few of the many amenities that make Talking Rock RV Resort stand out.
                    </div>
                    <div class="content_main_textBar">
                        <img class="img_home" src="images/home_photo.jpg" alt=""/>
                        The perfect affordable alternative to expensive mountain homes and cabins,<a href="the-community.php">Talking Rock RV Resort</a> offers you your very own<a href="property-listings.html">deeded RV site</a>, always ready for your getaways and seasonal stays. Leave your RV year-round if you choose.
                    </div>
                </div>
                <div class="content_foot">
                    If you are interested in more RV Resort Properties For Sale, please visit our <a href="http://www.ourlot.com/resortrvlots.php" target="_blank">Coosawattee River Resort Camper Lots</a> in mountain gated community with wonderful and varied amenities  to enjoy. The majority of our lots / sites are ready with septic, electric and water hookups.
                </div>
            </div>
            <div class="rightSideBar">
                <div class="rightSideBar_wrapper">
                    <div class="helpWrapper">
                        <div class="featured_property">
                            <?
                            require "manage/db_connection.php";
                            $dirThumb=str_replace("../","",$dirThumb);
                            $dirFull=str_replace("../","",$dirFull);
                            $sql="SELECT * FROM ".$prefix."_properties WHERE state='1'  ORDER BY id DESC LIMIT 0 , 15";
                            $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                            if($result AND mysql_num_rows($result)>0)
                            {
                                $i=0;
                                while($row=mysql_fetch_array($result))
                                {
                                    $ids[$i]=$row['id'];
                                    $i++;
                                }
                                $id=$ids[rand(0,(count($ids)-1))];
                            }
                            else
                            {
                                ?>
                                <div align="left"><br>
                                    <table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-top: 0px solid #000000;border-left: 0px solid #000000;border-right: 0px solid #000000;border-bottom: 0px solid #000000;" bordercolor="#000000">
                                        <tr>
                                            <td align="center"><font color="#ff0000">Your search returned no results.</font></td>
                                        </tr>
                                    </table>
                                </div>
                            <?
                            }
                            if($id)
                            {
                            $sql="SELECT * FROM ".$prefix."_properties WHERE state='1' AND id='".$id."' ORDER BY id DESC LIMIT 0 , 15";
                            $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                            if($result AND mysql_num_rows($result)>0)
                            {
                                $row=mysql_fetch_array($result);
                            }
                            ?>
                            <p class="featured_property_head">Featured Property</p>
                            <p class="featured_property_lot"><?=$row['Property_Name'];?></p>
                            <div class="featured_property_main">
                                <a class="mainAnchor" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>">
                                    <img class="mainImage" src="<?
                                        if($row['thumb'] AND file_exists($dirThumb.$row['thumb']))
                                        {
                                            echo $dirThumb.$row['thumb'];
                                        }
                                        else
                                        {
                                            echo $dirThumb."noThumb.jpg";
                                        }
                                    ?>" alt=""/>
                                </a>
                                <span class="mainID">Listing ID:
                                    <?=$row['id'];?>
                                    <br>
                                    <?=str_replace("For ","",$row['Status'])." Price";?>
                                    :
                                    <?
                                    if($row['Price']>0)
                                    {
                                        ?>
                                        $
                                        <?=number_format($row['Price'],2);?>
                                    <?
                                    }
                                    else
                                    {
                                        echo "Call";
                                    }
                                    ?>
                                </span>
                                <span class="description"><?=substr($row['Description'],0,35);?> ...</span>
                                <a class="viewDetails" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>"> <img src="images/btnMore.jpg" width="82" height="23" alt=""> </a>
                                <? } ?>
                            </div>
                        </div>
                        <div class="clickBar">
                            <a href="property-listings.html">Click Here to View More Propeties</a>
                        </div>
                        <div class="coosawattee">
                            <a href="http://www.ourlot.com/resortrvlots.php" target="_blank"><img src="images/RV-LOTS_coosawatee-lots.jpg" alt="RV LOTS in Coosawattee, GA" width="273" height="100" border="0"></a>
                        </div>
                        <div class="contactBar">
                            <p>Susan Moody</p>
                            <p>Woodland Realty</p>
                            <p>706-635-7272</p>
                            <p>1-800-809-9982</p>
                        </div>
                    </div>
                    <div class="formContact">
                    <form class="form" method="POST" action="formProcessor.php">
                        <div class="formwrapper">
                        <input type="hidden" name="formName" value="quick-contact">
                        <p class="form_head">Quick Contact</p>
                        <div class="formName">
                            <label for="formName">Name:</label> <input id="formName" name="Full_Name" type="text"/>
                        </div>
                        <div class="formAddress">
                            <label for="formAddress">Email Address:</label> <input id="formAddress" name="Email_Address" type="text"/>
                        </div>
                        <div class="formPhone">
                            <label for="formPhone">Phone:</label> <input id="formPhone" name="Phone_Number" type="text"/>
                        </div>
                        <div class="formMessage">
                            <label class="labelArea" for="Message">Message:</label> <textarea id="formMessage" rows="5" name="Message" cols="14" class="formArea"></textarea>
                        </div>
                        <div class="formSecurity">
                            <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                            <img class="securityImage" src="securimage_show.php">-->
<!--                            <input id="formSecurity" name="code" type="text"/>-->
<!--                            <label for="formSecurity">Security Code:</label>-->
                        </div>
                        <div class="button">
                            <input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image">
                        </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'foot.php' ; ?><!--****************FOOTER****************-->
</body>
</html>