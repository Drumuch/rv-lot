<html>

<head>
<title>Minnesota Family Resorts - MN Fishing Resort - Timber Trail is a premier four-season Northern Minnesota family resort.</title>
<meta name="Description" content="Minnesota family resort, located just seven miles from Ely, Minnesota, Timber Trail Lodge sits inbetween four interconnected lakes offering both motorized boating and  Boundary Waters canoeing experiences. We are surrounded by over one million acres of Minnesota wilderness." />
<meta name="Keywords" content="minnesota family resorts, fishing in minnesota, mn fishing, , minnesota fishing, musky fishing in minnesota, minnesota fishing resort, northern minnesota resort, minnesota fishing vacation, boundary waters canoe area, minnesota fishing resorts, family resort minnesota, northern minnesota fishing, lakes resort minnesota, minnesota cabins, minnesota romantic getaway" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" href="/favicon.ico" />
<style>
<!--
table        { font-family: Arial; font-size: 10pt; color: #002D13 }
form { margin:0; padding:0; } 
a:link       {
	color: #445804;
	text-decoration: none;
}
a:visited    {
	color: #445804;
	text-decoration: none;
}
a:hover      {
	color: #800000;
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
.style3 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 21pt;
	color: #FAF305;
}
.style8 {color: #0A572D}
.style9 {font-size: 10px}
.style11 {font-weight: bold}
.style15 {font-weight: bold; font-size: 12pt; }
.style16 {font-weight: bold; font-size: 10pt; }
.style17 {font-family: Geneva, Arial, Helvetica, sans-serif}
.style18 {
	color: #000000;
	font-weight: bold;
}
.style19 {
	font-size: 12pt;
	font-style: italic;
	font-weight: bold;
}
.style20 {
	font-size: 12pt;
	font-style: italic;
}
.style21 {font-size: 14pt}
-->
</style>
<script language=javascript> function clearme(p,q) { if (q==1) { if(document.mform.txtMessage.value=='- Your Message Goes Here -') 	{ document.mform.txtMessage.value=''; }
} if (q==2) { if(document.mform.T2.value=='- Put Security Code -') { document.mform.T2.value=''; } }  	} </script>
<!--[if lt IE 7.]>
<script defer type="text/javascript" src="pngfix.js"></script>
<![endif]-->
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#BCCDAE" leftmargin="0" topmargin="0" style="background-image: url('images/bg.jpg'); background-repeat: repeat-x; background-position-y: top" onLoad="MM_preloadImages('images/btnHome-over.jpg','images/btnAccommodations-over.jpg','images/btnPackages-over.jpg','images/btnThings-over.jpg','images/btnWinter-over.jpg','images/btnBoatRental-over.jpg','images/btnFishing-over.jpg','images/btnReunions-over.jpg','images/btnGuidedFishing-over.jpg','images/btnAbout-over.jpg','images/btnDirections-over.jpg','images/btnContact-over.jpg','images/btnInTheNews-over.jpg','images/btnLinks-over.jpg','images/btnLakeMaps-over.jpg')">
<!--Online Editor Marker. Do Not Remove-->
<div align="center">
	<table border="0" cellpadding="0" style="border-collapse: collapse" width="898" id="table1">
		<tr>
			<td colspan="4" height="6">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="898" height="15" id="table9">
					<tr>
						<td width="913" height="24" colspan="3">&nbsp;						</td>
					</tr>
					<tr>
						<td width="15" height="6">
						<img border="0" src="images/shtop1.png" width="15" height="6"></td>
						<td width="883" background="images/shtop.png" height="6">
						<img border="0" src="images/spacer.gif" width="1" height="1"></td>
						<td width="15" height="6">
						<img border="0" src="images/shtop2.png" width="14" height="6"></td>
					</tr>
				</table>
			</div>			</td>
		</tr>
		<tr>
			<td width="6" background="images/shleft.png" rowspan="2">&nbsp;</td>
			<td width="601" height="98" background="images/bgTop.jpg"><blockquote>
			  <p><span class="style3">Timber Trail Lodge</span><br>
			    <span class="style8">A Premier Minnesota Vacation Resort in Ely Minnesota<br>
		      Northeastern Minnesota's Boundary Waters Canoe Area Wilderness</span></p>
		   </blockquote>		   </td>
			<td width="299" background="images/bgTop.jpg"><div align="right">
			  <blockquote>
			    <p class="style3">1-800-777-7348</p>
		      </blockquote>
			</div></td>
			<td width="2" rowspan="2" background="images/shright.png">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="886" id="table2" bgcolor="#F5F7E7">
					<tr>
						<td height="13">
						<img border="0" src="images/wood.jpg" width="886" height="13"></td>
					</tr>
					<tr>
						<td height="285">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="886" height="285" id="table3">
								<tr>
									<td width="363"><img border="0" src="images/Logo.jpg" width="363" height="285"></td>
									<td width="500">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="500" id="table4">
											<tr>
												<td height="3">
												<img border="0" src="images/bannerTop.jpg" width="500" height="3"></td>
											</tr>
											<tr>
												<td height="278"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="500" height="278">
                                                  <param name="movie" value="banner.swf">
                                                  <param name="quality" value="high">
                                                  <embed src="banner.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="500" height="278"></embed>
											    </object></td>
											</tr>
											<tr>
												<td height="4">
												<img border="0" src="images/bannerbottom.jpg" width="500" height="4"></td>
											</tr>
										</table>
									</div>									</td>
									<td width="23">
									<img border="0" src="images/bannerright.jpg" width="23" height="285"></td>
								</tr>
							</table>
						</div>						</td>
					</tr>
					<tr>
						<td height="13">
						<img border="0" src="images/wood.jpg" width="886" height="13"></td>
					</tr>
					<tr>
						<td background="images/bgForm.jpg" height="47" width="29">
						<form method="POST" action="sendmail.php" name="mform">
						  <div align="center">
<table border="0" cellpadding="0" style="border-collapse: collapse" width="886" height="47" id="table11">
                              <tr>
                                <td width="11">&nbsp;</td>
                                <td width="27"><img border="0" src="images/icofrm.jpg" width="27" height="20"></td>
                                <td width="139"><img border="0" src="images/quickcontact.jpg" width="137" height="47"></td>
                                <td><span style="font-size: 10px"><strong>Your 
                                  Name</strong></span><br>
                                                <input name="txtName" type="text" style="font-size: 9pt; font-family: Arial; border: 1px solid #999999" size="12"></td>
                                <td><span style="font-size: 10px"><strong>Your Email</strong></span><br>
                                    <input type="text" name="txtEmail" size="12" style="font-size: 9pt; font-family: Arial; border: 1px solid #999999"></td>
                                <td><span style="font-size: 10px"><strong>Contact  Phone</strong></span><br>
                                    <input type="text" name="txtPhone" size="12" style="font-size: 9pt; font-family: Arial; border: 1px solid #999999"></td>
                                <td><textarea name="txtMessage" id="textarea" cols="22" rows="2" style="font-size: 9pt; font-family: Arial; height: 35; border: 1px solid #999999; width:140" onFocus="clearme(this.value,'1')">- Your Message Goes Here -</textarea></td>
                                <td align=center><span style="font-size: 10px"><strong>Add me to Mailing List</strong></span>
                                    <input type="checkbox" value="Add my email to Mailing List = YES" name="txtMailing" checked></td>
                                <td width="128"><img src="security-image.php?width=144" width="116" height="19" alt="Security Code" /><br>
                                    <input type="text" name="T2" size="17" style="font-size: 9pt; font-family: Arial; border: 1px solid #999999" value="- Put Security Code -" onFocus="clearme(this.value,'2')"></td>
                                <td width="96"><input border="0" src="images/btnSend.jpg" name="I1" width="80" height="28" type="image"></td>
                              </tr>
                            </table>
					      </div>
						</form>						</td>
					</tr>
					<tr>
						<td height="13">
						<img border="0" src="images/spacer.gif" width="1" height="1"></td>
					</tr>
					<tr>
						<td>
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="886" id="table5">
							  <tr>
									<td width="2">&nbsp;</td>
									<td width="266" valign="top">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="238" id="table6" background="images/bgmenu.jpg">
											<tr>
												<td height="3">
												<img border="0" src="images/menutop.jpg" width="238" height="3"></td>
											</tr>
											<tr>
												<td height="3">
												<img border="0" src="images/spacer.gif" width="1" height="1"></td>
											</tr>
											<tr>
												<td height="34"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image36','','images/btnHome-over.jpg',1)"><img src="images/btnHome.jpg" name="Image36" width="238" height="34" border="0"></a></td>
										  </tr>
											<tr>
												<td><a href="accommodations.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image37','','images/btnAccommodations-over.jpg',1)"><img src="images/btnAccommodations.jpg" name="Image37" width="238" height="31" border="0"></a></td>
										  </tr>
											<tr>
												<td><a href="specials.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image38','','images/btnPackages-over.jpg',1)"><img src="images/btnPackages.jpg" name="Image38" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="ely-resort-activities.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image39','','images/btnThings-over.jpg',1)"><img src="images/btnThings.jpg" name="Image39" width="238" height="27" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="winter.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','','images/btnWinter-over.jpg',1)"><img src="images/btnWinter.jpg" name="Image40" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="boat-rental.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','images/btnBoatRental-over.jpg',1)"><img src="images/btnBoatRental.jpg" name="Image41" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="fishing.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','images/btnFishing-over.jpg',1)"><img src="images/btnFishing.jpg" name="Image42" width="238" height="29" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="reunionsretreats.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image43','','images/btnReunions-over.jpg',1)"><img src="images/btnReunions.jpg" name="Image43" width="238" height="30" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="resortguide.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image44','','images/btnGuidedFishing-over.jpg',1)"><img src="images/btnGuidedFishing.jpg" name="Image44" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
											  <td><a href="mediapage.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image48','','images/btnInTheNews-over.jpg',1)"><img src="images/btnNews.jpg" name="Image48" width="238" height="29" border="0"></a></td>
										  </tr>
											<tr>
											  <td><a href="links_page.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/btnLinks-over.jpg',1)"><img src="images/btnLinks.jpg" name="Image49" width="238" height="30" border="0"></a></td>
										  </tr>
											<tr>
											  <td><a href="Lake_Maps.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/btnLakeMaps-over.jpg',1)"><img src="images/btnLakeMaps.jpg" name="Image50" width="238" height="31" border="0"></a></td>
										  </tr>
											<tr>
												<td><a href="ely-resorts-why.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image45','','images/btnAbout-over.jpg',1)"><img src="images/btnAbout.jpg" name="Image45" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="directions.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image46','','images/btnDirections-over.jpg',1)"><img src="images/btnDirections.jpg" name="Image46" width="238" height="30" border="0"></a></td>
											</tr>
											<tr>
												<td><a href="resortcontact.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/btnContact-over.jpg',1)"><img src="images/btnContact.jpg" name="Image47" width="238" height="31" border="0"></a></td>
											</tr>
											<tr>
												<td align="center"><p>&nbsp;</p>
												  <p>&nbsp;</p>
											    <p><a href="pdf/TimberTrailBrochure-opt.pdf" target="_blank"><img border="0" src="images/viewBrochure.jpg" width="148" height="202"></a></p></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td align="center"><a href="webcam.htm"><img border="0" src="images/viewWebcam.jpg" width="148" height="121"></a></td>
											</tr>
											<tr>
												<td><div align="center">
												  <p>&nbsp;</p>
												  <blockquote>
												    <p>&nbsp;</p>
												    <!-- boundary -->
												  <p align="center"> <strong>Ely Minnesota<br>
                                                    </strong> <strong>is rated one of the<br>
                                                    </strong><strong>Top Fishing                                     Towns in America</strong><br>
                                                    <em><strong>by Field &amp; Stream Magazine</strong></em></p>
												  <p align="center">&nbsp;</p>
												  <p align="center"><strong>Men's Journal <br>
&quot;25 Dream Towns and Secret Destinations&quot; <br>
											      </strong>Article on Timber Trail Lodge and Ely, Minnesota</p>
												  <p align="center">&nbsp;</p>
												</div>
											      <div align="center"><span class="style21"><strong><img src="images/activities-fishing.jpg" alt="fishing-in-minnesota" width="200" height="150" border="2" align="absmiddle"></strong></span></div></td>
											</tr>
											<tr>
												<td height="4">
												<img border="0" src="images/menuBottom.jpg" width="238" height="4"></td>
											</tr>
										</table>
									</div>								  </td>
								<td width="2">&nbsp;</td>
								  <td colspan="2" valign="top" bgcolor="#F5F7E7"><p><img src="images/Welcome.jpg" alt="Welcome to Timber Trail Lodge Ely Minnesota's Family Resort" width="508" height="44"></p>
								    <blockquote>
									  <table width="521" border="0">
                                        <tr>
                                          <td width="322"><p align="center" class="style11">Fishing . Paddling . Relaxing <br>
                                            Summer . Fall . Winter . Spring</p>
                                            <p align="center" class="style15">Modern to Deluxe                   Accommodations on White Iron Chain of Lakes in Ely, Minnesota</p>
                                            <p align="center" class="style16">Free Wireless Internet Available in our Lodge </p>
                                          <p align="left"></p></td>
                                          <td width="189"><a href="month.htm"><img src="images/resort-specials.jpg" alt="Last Minute Resort Specials" width="178" height="177" border="0"></a></td>
                                        </tr>
                                      </table>
									  <p align="left"><span class="style11"><b><em><img src="images/Cabin-EG01_1.jpg" width="304" height="233" hspace="7" align="left">Timber Trial Lodge</em><br>
							          </b></span>is a premier four-season Northern Minnesota family resort. Located just seven miles from Ely, Minnesota, Timber Trail Lodge sits on the south shore of Farm Lake, one of four interconnected lakes offering both motorized boating and <a href="http://www.boundarywatersoutfitters.com" target="_blank">Boundary Waters</a> canoeing experiences. We are surrounded by over one million acres of wilderness.<br>
							          <br>
								        <br>
								        It is an <a href="ely-resort-activities.html">Ely Minnesota family resort</a> offering modern accommodations with direct Boundary Waters access for those who wish to incorporate a wilderness adventure into their resort vacation. See <a href="accommodations.htm">accommodations</a> for information on our modern to deluxe cabins.<span class="style11"><br>
									    </span></p>
									  <p align="center"><strong>Our   Diamond Willow Cabin was  featured on Great American Lodges -- PBS</strong></p>
									  <p align="center"> <strong>  read more&gt;&gt;<a href="mediapage.htm"> A winter special featuring Wintergreen Dogsledding                     Lodge and Timber Trail Lodge </a> </strong> </p>
									  <p align="left">&nbsp; </p>
									  <p align="left" class="style19">Why Ely, Minnesota and Timber Trail Lodge?</p>
									  <p align="left"><strong>"A                        Minnesota Vacation Resort Since 1939"</strong></p>
									  <p align="left">Timber                        Trail Lodge, a premier <a href="index.php">Ely Minnesota             family resort</a>&nbsp;located on the                        edge of the Boundary Waters Canoe Area Wilderness,                        has been lodging and outfitting wilderness visitors                        since 1939. Our experience and dedication to our                        customers makes us one of the Ely area's premier                        hospitality companies.   &nbsp; <strong> <a href="ely-resorts-why.htm"> Why <em>this</em> Minnesota Family Resort?</a></strong></p>
									  <table width="454" border="0" align="center">
                                        <tr>
                                          <td width="448" bgcolor="#FFFFCC"><p align="center" class="style15">Minnesota Resorts Current Events:</p>
                                            <p align="center"> <strong> -------------------------------------------------------------</strong></p>
                                          <p align="center"><strong>Thank you to everyone for a great year in 2009!</strong></p>
                                          <p align="center">We are now taking reservations for Summer 2010!</p>
                                          <p align="center">Book now for the best rates and availability.</p>
                                          <p align="center">Check out these links or call us for your Ely, Minnesota Vacation!</p>
                                          <p align="center"><a href="http://www.timbertrail.com/accommodations.htm">Accommodations </a>| <a href="http://www.timbertrail.com/month.htm">Last Minute Offers</a></p>
                                          <p align="center">800-777-7348</p></td>
                                        </tr>
                                      </table>
									  <p align="left">&nbsp;</p>
									  <p align="left"><b><span class="style20">Ely Minnesota Family Resorts - Minnesota Vacation Resort</span></b></p>
									  <p align="left">Family vacations at a Northern Minnesota resort have been a tradition for generations. Here at Timber Trail Lodge we have been providing family vacations since our founding in 1939.<br>
									    <br>
									    <br>
								        <img src="images/activities1-hiking.jpg" width="204" height="154" hspace="7" align="left">Minnesota is home to some One Thousand family operated resorts, each offering amenities that can vary greatly from resort to resort. Ely Minnesota is unique in that we are located in the Northern most portion of Minnesota, and have access to the Boundary Waters Wilderness Canoe Area. The Boundary Waters is the largest wilderness east of the Rockies, and was selected as one of the &quot;fifty places in the world a person should visit in their lifetime&quot; by National Geographic Magazine. Many families seek to include a Boundary Waters experience in their family vacation, and here at Timber Trail we offer both day and overnight canoe trips into the Boundary Waters. </p>
									  <h2 align="left" class="style21"><strong>More Information about Timber Trail Lodge...</strong> </h2>
									  <h2 align="left"><span class="style21"><a href="ely-resorts-why.htm" class="style20">Minnesota Resorts... </a></span> </h2>
									  <p align="left">  Ely,                                        Minnesota Resorts information.&nbsp; How did Ely,                                        Minnesota Resorts start and where are they today.&nbsp;                                        Ely has long been a fishing and resort/lodge destination                                        for people from all around the world. <strong><br>
                                          <a href="ely-resorts-why.htm"> Learn more about Ely,                                        Minnesota Resorts. </a> </strong> </p>
									  <p align="left"> <a href="specials.htm" class="style20"> Packages, Specials and Custom Trips... </a> </p>
									  <p align="left">  Timber                                        trail has packages and specials for every season of the                                        year.&nbsp; Our packages and custom trips range from                                        Minnesota fishing resort trips to romance and honeymoon                                        getaways.&nbsp;<strong> <a href="specials.htm"> See all of our Minnesota resorts specials and custom trips information. </a> </strong> </p>
									  <p align="left"> <a href="ely-resort-activities.html" class="style20"> Ely                                        Minnesota Resort Amenities... </a> </p>
									  <p align="left">  Our Ely, Minnesota resort offers various unique amenities for any time of the year.&nbsp;<strong> <a href="ely-resort-activities.html"> View our Minnesota resorts amenities by clicking on this link. </a> </strong> </p>
									  <p align="left"><br>
									  </p>
								    </blockquote></td>
								</tr>
								<tr>
									<td width="2">&nbsp;</td>
									<td colspan="4">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="761" id="table7">
											<tr>
												<td height="21" width="2">&nbsp;</td>
												<td height="21" colspan="7" background="images/adTop.jpg">&nbsp;</td>
												<td height="21" width="12">&nbsp;</td>
											</tr>
											<tr>
												<td height="210" width="2">&nbsp;</td>
												<td height="210" width="2" background="images/bgAd.jpg">&nbsp;</td>
												<td height="210" width="243" background="images/bgAd.jpg"><a href="http://www.boundarywatersoutfitters.com" target="_blank"><img border="0" src="images/Ad1.jpg" width="243" height="200"></a></td>
												<td height="210" width="3" background="images/bgAd.jpg">&nbsp;</td>
												<td height="210" width="243" background="images/bgAd.jpg"><a href="ely-resort-activities.html"><img border="0" src="images/Ad2.jpg" width="243" height="200"></a></td>
												<td height="210" width="3" background="images/bgAd.jpg">&nbsp;</td>
												<td height="210" width="244" background="images/bgAd.jpg"><a href="fishing.html"><img border="0" src="images/Ad3.jpg" width="243" height="200"></a></td>
												<td height="210" width="2" background="images/bgAd.jpg">&nbsp;</td>
												<td height="210" width="12">&nbsp;</td>
											</tr>
											<tr>
												<td width="2">
												<img border="0" src="images/spacer.gif" width="1" height="1"></td>
												<td bgcolor="#E8EAD4" height="15" colspan="7">
												<img border="0" src="images/spacer.gif" width="1" height="1"></td>
												<td width="12">
												<img border="0" src="images/spacer.gif" width="1" height="1"></td>
											</tr>
											<tr>
												<td width="2">&nbsp;</td>
												<td width="2" bgcolor="#E8EAD4">&nbsp;</td>
												<td width="243" bgcolor="#E8EAD4" valign="top"><p><span class="style17">Boundary Waters Outfitters is a full-service canoe trip                                        outfitter servicing all Ely area entry points, with direct                                        Boundary Waters access.&nbsp; We offer full and partial                                        outfitting services, guide services and outfitting packages for                                        adventure travelers, family groups, non-profit groups, and                                        fishing packages.</span><br>
  &nbsp;</p>
											      <!--Boundary Links--></td>
												<td width="3" bgcolor="#E8EAD4">&nbsp;</td>
												<td width="243" bgcolor="#E8EAD4" valign="top"><p><span class="style17">What is there to do in Ely Minnesota?&nbsp; To     								help you out, we have put a list of things     								together just for you!</span></p>
											    <p align="center"><img src="images/Diamond-Willow-300.jpg" width="150" height="100"></p>
											    <p align="center">&nbsp;</p>
											    <p align="center"><strong><a href="ely-resort-activities.html">More on ELY RESORT ACTIVITIES</a></strong></p></td>
												<td width="3" bgcolor="#E8EAD4">&nbsp;</td>
												<td bgcolor="#E8EAD4" valign="top"><p align="left"><b>Minnesota Fishing Resorts</b>. Timber Trail Lodge offers both Boundary  Waters fishing trips and access to four interconnected lakes for guided  and self-guided fishing trips. Our primary species include Walleye,  Bass, Northern and Crappies. Our lakes offer water with no motor  restrictions, and availability to both canoe and motorized Boundary  Waters fishing. Our guides spend over 150 days each summer fishing  northern Minnesota lakes and are available for guided trips and for  answering any questions you may have regarding fishing our lakes and  the surrounding Boundary Waters area.</p>
											    <p align="left"><a href="fishing.html"><strong>More on FISHING and FISHING PACKAGES</a></strong></p></td>
												<td width="2" bgcolor="#E8EAD4">&nbsp;</td>
												<td width="12">&nbsp;</td>
											</tr>
											<tr>
												<td width="2">&nbsp;</td>
												<td bgcolor="#E8EAD4" colspan="7" height="21"><p align="center">&nbsp;</p>											    </td>
												<td width="12">&nbsp;</td>
											</tr>
										</table>
									</div>									</td>
								</tr>
								<tr>
									<td width="2">&nbsp;</td>
									<td width="266">&nbsp;</td>
									<td width="2">&nbsp;</td>
									<td width="593">&nbsp;</td>
									<td width="11">&nbsp;</td>
								</tr>
							</table>
						</div>						</td>
					</tr>
					<tr>
						<td align="center">
						<img border="0" src="images/seperator.jpg" width="775" height="3"></td>
					</tr>
					<tr>
						<td><div align="center">
						  <p>Timber Trail Lodge, LLC <br>
						    629 Kawishiwi Trail - Ely, Minnesota 55731 <br>
						    Tel: 218-365-4879 or 800-777-7348<br>
					        <a href="mailto:bill@timbertrail.com">bill@timbertrail.com</a></p>
						  <p>&nbsp;</p>
					  </div></td>
					</tr>
					<tr>
						<td align="center"><a href="index.php">Home</a> |
						<a href="accommodations.htm">Accommodations &amp; 
						Rates</a> | <a href="specials.htm">
						Packages &amp; Specials</a> | <a href="ely-resort-activities.html">
						Things To Do</a> | <a href="winter.html">Winter</a> |
						<a href="boat-rental.html">Boat Rental</a><br>
						<a href="fishing.html">Fishing</a> |
						<a href="reunionsretreats.html">Reunions &amp; Retreats</a> 
						| <a href="resortguide.html">Guided Fishing 
						Trips</a> | <a href="mediapage.htm">In the News</a> |
						<a href="links_page.htm">Links</a> |
						<a href="Lake_Maps.htm">Lake Maps</a><br>
						<a href="ely-resorts-why.htm">About Our Resort</a> |
						<a href="directions.htm">Directions</a> |
						<a href="resortpolicies.htm">Resort Policies</a> |
						<a href="resortcontact.html">Contact Us</a><p class="style9">						�  Timber Trail Lodge. 
						All Rights Reserved.                        
						<p class="style9">� <a href="http://www.budget-seo.com" target="_blank">Web work and seo</a> by Budget-SEO.com </td>
					</tr>
					<tr>
						<td class="style9">&nbsp;</td>
					</tr>
				</table>
			</div>			</td>
		</tr>
		<tr>
			<td height="6" colspan="4" class="style9">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="898" id="table10">
					<tr>
						<td width="15" height="6">
						<img border="0" src="images/shbot1.png" width="15" height="6"></td>
						<td background="images/shbottom.png" width="883" height="6">
						<img border="0" src="images/spacer.gif" width="1" height="1"></td>
						<td width="15" height="6">
						<img border="0" src="images/shbot2.png" width="15" height="6"></td>
					</tr>
				</table>
			</div>			</td>
		</tr>
	</table>
	<p class="style9"></div>
<!--Online Editor Marker. Do Not Remove-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-983525-17");
pageTracker._trackPageview();
} catch(err) {}</script></body></html>
