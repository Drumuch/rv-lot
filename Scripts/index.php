<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RV Lots</title>
<style>
<!--
table        { font-family: Tahoma; font-size: 11pt; line-height: 150% }
.txtbox      { color: #000000; width: 135; border: 1px solid #C9B993; background-color: 
               #DDD0B2 }
a            { color: #D1DC76; font-family: Verdana; font-size: 10pt; text-decoration: none; 
               font-weight: bold }
a:visited    {
	color: #630;
	font-family: Verdana;
	font-size: 10pt;
	text-decoration: none;
	font-weight: bold
}
a:hover      {
	color: #91D4A1
}
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18pt;
	color: #000000;
}
.style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11pt;
}
.style9 {color: #83C993}
.style10 {font-family: Arial, Helvetica, sans-serif}
.style15 {font-family: Arial, Helvetica, sans-serif; font-size: 10pt; }
.style17 {
	font-size: 10pt;
	font-weight: bold;
}
.style45 {
	font-size: 18pt;
	font-family: Arial, Helvetica, sans-serif;
	color: #5a7e41;
}
.style26 {	font-size: 16pt;
	font-style: italic;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #425834;
}
.style29 {	color: #425834;
	font-style: italic;
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 16px;
}
.style47 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #5a7e41;
}
a:link {
	color: #630;
}
a:active {
	color: #630;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>
<body bgcolor="#F6EFDF" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="background-position: center top; background-repeat:no-repeat" onLoad="MM_preloadImages('images/btnHome_over.jpg','images/btnPropertyListings_over.jpg','images/btnCommunity_over.jpg','images/btnGallery_over.jpg','images/btnContact_over.jpg')">
<div align="center">
	<table border="0" cellpadding="0" style="border-collapse: collapse" width="100%">
		<tr>
			<td background="images/bgTop.jpg" height="510">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="510">
					<tr>
						<td height="73">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="117">
								<tr>
									<td height="73">
									<font color="#FFFFFF" size="7">North Georgia RV Lots</font></td>
								</tr>
								<tr>
									<td height="44">						<img border="0" src="images/logo.jpg" width="324" height="44"></td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
					<tr>
						<td height="35">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="35">
								<tr>
									<td width="78"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image28','','images/btnHome_over.jpg',1)"><img src="images/btnHome.jpg" name="Image28" width="78" height="35" border="0"></a></td>
								  <td width="17">&nbsp;</td>
									<td width="163"><a href="property-listings.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image29','','images/btnPropertyListings_over.jpg',1)"><img src="images/btnPropertyListings.jpg" name="Image29" width="163" height="35" border="0"></a></td>
								  <td width="163"><a href="the-community.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image30','','images/btnCommunity_over.jpg',1)"><img src="images/btnCommunity.jpg" name="Image30" width="163" height="35" border="0"></a></td>
								  <td width="99"><a href="gallery.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','images/btnGallery_over.jpg',1)"><img src="images/btnGallery.jpg" name="Image31" width="99" height="35" border="0"></a></td>
								  <td width="119"><a href="contact-us.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image32','','images/btnContact_over.jpg',1)"><img src="images/btnContact.jpg" name="Image32" width="119" height="35" border="0"></a></td>
								  <td width="344">&nbsp;</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
					<tr>
						<td height="358">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="358">
								<tr>
									<td width="130" valign="top">
									<img border="0" src="images/bannerLeft.jpg" width="130" height="358"></td>
									<td width="601" valign="top">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="601" height="358">
											<tr>
												<td height="57">
												<img border="0" src="images/bannerTop.jpg" width="601" height="57"></td>
											</tr>
											<tr>
											  <td height="264"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','601','height','264','src','banner','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','banner' ); //end AC code
</script>
                                              </td>
											</tr>
											<tr>
												<td height="37">
												<img border="0" src="images/bannerBottom.jpg" width="601" height="37"></td>
											</tr>
										</table>
									</div>
									</td>
									<td width="16" valign="top">
									<img border="0" src="images/bannerRight.jpg" width="17" height="358"></td>
									<td width="235" valign="top">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="235" height="358">
											<tr>
												<td height="71" colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td height="137" background="images/wintop.jpg" colspan="3">
												<div align="center">
													<table border="0" cellpadding="0" style="border-collapse: collapse" width="235" height="137">
														<tr>
															<td colspan="3" height="20"></td>
														</tr>
														<tr>
															<td width="17">&nbsp;</td>
															<td width="199" valign="top"><p><span class="style26">Welcome to RV Lots</span></p>
															  <p align="center"> <span class="style29">Only 5 minutes from <a href="http://carters.sam.usace.army.mil/">Carters Lake </a> Marina</span></p>
															  <p style="line-height: 1; color: #73975A;">
															<p style="line-height: 1">                                                                                                                       
														  <p style="line-height: 1"></td>
															<td width="19">&nbsp;</td>
														</tr>
														<tr>
															<td colspan="3" height="15"></td>
														</tr>
													</table>
												</div>
												</td>
											</tr>
											<tr>
												<td height="113" width="17" rowspan="3">
												<img border="0" src="images/winleft.jpg" width="17" height="113"></td>
												<td height="38" width="200">
												<a href="site-plan.html">												<img border="0" src="images/siteMap.jpg" width="200" height="38"></a><img border="0" src="images/wincnt.jpg" width="200" height="10"></td>
												<td height="113" width="18" rowspan="3">
												<img border="0" src="images/winright.jpg" width="18" height="113"></td>
											</tr>
											<tr>
												<td height="37" width="200">
												<a href="directions-and-map.html">
												<img border="0" src="images/directions.jpg" width="200" height="37"></a></td>
											</tr>
											<tr>
												<td height="28" width="200">
												<img border="0" src="images/winbottom.jpg" width="200" height="28"></td>
											</tr>
											<tr>
												<td height="37" colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td height="300" valign="top" align="center">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="983">
					<tr>
						<td width="982" colspan="5" height="40">&nbsp;</td>
					</tr>
					<tr>
						<td width="80">&nbsp;</td>
						<td width="586" valign="top"><h2><span class="style45">RV lots For Sale</span> </h2>						  
						<p class="style1">&nbsp; </p>
						  <p align="left" class="style8"><span class="style1"><img src="images/home_photo_2.jpg" width="248" height="171" hspace="5" align="left"></span>Talking Rock RV Resort, located just west of Ellijay GA, offers the opportunity to own your very own deeded RV site in the beautiful mountain foothills of north Georgia. Only 5 minutes from 3,500 acre <a href="http://carters.sam.usace.army.mil/" target="_blank" class="style9">Carters Lake </a>, considered by many to be Georgia's most spectacular and serene mountain lake. Well stocked with striped bass, hybrids and walleye, it is a fisherman's paradise. </p>
						  <p align="left" class="style8">&nbsp;</p>
						  <p align="left" class="style8">&nbsp;</p>
						  <p align="left" class="style8"><img src="images/home_photo_3.jpg" width="256" height="179" hspace="5" align="right">With this comes a full amenity package including an all new clubhouse. Pool, tennis, pavilion, walking trails with excercise stations, excercise room, shower and laundry facilities, and modern playground equipment are just a few of the many amenities that make Talking Rock RV Resort stand out. </p>
						  <p align="left" class="style8">&nbsp;</p>
						  <p align="left" class="style8">&nbsp;</p>
						  <p align="left" class="style8"><img src="images/home_photo.jpg" width="248" height="171" hspace="5" align="left">The perfect affordable alternative to expensive mountain homes and cabins, Talking Rock RV Resort offers you your very own deeded RV site, always ready for your getaways and seasonal stays. Leave your RV year-round if you choose. </p>
						  <p align="left">&nbsp;</p>						  
						  <p>
						  <p>&nbsp;</p>
						<p></td>
						<td width="37">&nbsp;</td>
						<td width="273" valign="top">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="273">
								<tr>
									<td height="9">
									<img border="0" src="images/fpTop.jpg" width="273" height="9"></td>
								</tr>
								<tr>
									<td width="273">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="273">
											<tr>
												<td width="4" background="images/fpLeft.jpg" style="background-image: url('images/fpLeft.jpg'); background-repeat: repeat-y; background-position-x: left">&nbsp;</td>
												<td width="17">&nbsp;</td>
												<td width="233"><span class="style47">Featured 
												Property</span>






<!-- FEATURED LISTING STARTING HERE -->
<?
require "admin/db_connection.php";
$dirThumb=str_replace("../","",$dirThumb);
$dirFull=str_replace("../","",$dirFull);
	$sql="SELECT * FROM ".$prefix."_properties WHERE state='1'  ORDER BY id DESC LIMIT 0 , 15";
	$result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
	if($result AND mysql_num_rows($result)>0)
		{
		$i=0;
		while($row=mysql_fetch_array($result))
			{
			$ids[$i]=$row['id'];
			$i++;
			}
		$id=$ids[rand(0,(count($ids)-1))];
		}
	else
		{
		?>
						<div align="left"><br>
						<table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-top: 0px solid #000000;border-left: 0px solid #000000;border-right: 0px solid #000000;border-bottom: 0px solid #000000;" bordercolor="#000000">
							<tr>
								<td align="center"><font color="#ff0000">Your search returned no results.</font></td>
							</tr>
						</table>

						</div>
		<?
		}
		if($id)
			{
			$sql="SELECT * FROM ".$prefix."_properties WHERE state='1' AND id='".$id."' ORDER BY id DESC LIMIT 0 , 15";
			$result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
			if($result AND mysql_num_rows($result)>0)
				{
				$row=mysql_fetch_array($result);
				}
?>
			<div align="left"  style="font-size:14px;font-weight:bold;color:#000000;">
<?=$row['Property_Name'];?><a href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>"><p align="right"><img align="right" src="<?
if($row['thumb'] AND file_exists($dirThumb.$row['thumb']))
	{
	echo $dirThumb.$row['thumb'];
	}
else
	{
	echo $dirThumb."noThumb.jpg";
	}
?>" border="0" style="border-width:1px; border-color:#000000;" hspace="3" vspace="3"></a></p>
<span style="font-size:12px;font-weight:bold;">Listing ID: <?=$row['id'];?><br>
<span style="font-size:12px;font-weight:bold;"><?=str_replace("For ","",$row['Status'])." Price";?>: <?
if($row['Price']>0)
	{
	?>
$<?=number_format($row['Price'],2);?>
	<?
	}
else
	{
	echo "Call";
	}
?></span>
<span style="font-size:12px;font-weight:normal;">
<?=substr($row['Description'],0,35);?> ...
<a class="viewDetails" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>">
<img border="0" src="images/btnMore.jpg" width="82" height="23">
</a></span>


<? } ?>
<!-- FEATURED LISTING ENDING HERE -->








</td>
												<td width="15">&nbsp;</td>
												<td width="4" background="images/fpRight.jpg" style="background-image: url('images/fpRight.jpg'); background-repeat: repeat-y; background-position-x: right">&nbsp;</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
								<tr>
									<td height="8">
									<img border="0" src="images/fpBottom.jpg" width="273" height="8"></td>
								</tr>
								<tr>
									<td height="20">&nbsp;</td>
								</tr>
								<tr>
									<td>
									<img border="0" src="images/grbx.jpg" width="273" height="30"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td height="9">
									<img border="0" src="images/adrTop.jpg" width="273" height="9"></td>
								</tr>
								<tr>
									<td>
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="273" bgcolor="#DDD0B2">
											<tr>
												<td width="4" background="images/fpLeft.jpg" style="background-image: url('images/adrLeft.jpg'); background-repeat: repeat-y; background-position-x: left">&nbsp;</td>
												<td width="10">&nbsp;</td>
												<td width="230" align="center">												  <div align="center">
												  <p><b><span class="style15">Wanda Blalock Century 21 Best 
								                  Mountain Properties </span></b></p>
												  <p class="style10"><span class="style17">(706) 
						                          253-3707 </span></p>
												  <p><span class="style15"><b>(770) 894-1059</b></span><span class="style15"><strong><br>
</strong></span></p>
											  </div></td>
												<td width="25">&nbsp;</td>
												<td width="4" background="images/fpRight.jpg" style="background-image: url('images/adrRight.jpg'); background-repeat: repeat-y; background-position-x: right">&nbsp;</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
								<tr>
									<td>
									<img border="0" src="images/adrBottom.jpg" width="273" height="9"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td height="9">
									<img border="0" src="images/fpTop.jpg" width="273" height="9"></td>
								</tr>
								<tr>
									<td>
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="273">
											<tr>
												<td width="4" background="images/fpLeft.jpg" style="background-image: url('images/fpLeft.jpg'); background-repeat: repeat-y; background-position-x: left">&nbsp;</td>
												<td width="10">&nbsp;</td>
												<td width="240">
												<form method="POST" action="formProcessor.php"><input type="hidden" name="formName" value="quick-contact">
													<p><b>Quick Contact</b></p>
													<div align="center">
														<table border="0" cellpadding="0" style="border-collapse: collapse" width="240">
															<tr>
																<td align="right" width="97" height="40">
																<font size="2">
																Name:</font></td>
																<td width="7" height="43">&nbsp;</td>
																<td height="43">
																<input type="text" name="Full_Name" size="20" class="txtbox"></td>
															</tr>
															<tr>
																<td align="right" width="97" height="40">
																<font size="2">
																Email Address:</font></td>
																<td width="7" height="36">&nbsp;</td>
																<td height="36">
																<input type="text" name="Email_Address" size="20" class="txtbox"></td>
															</tr>
															<tr>
																<td align="right" width="97" height="40">
																<font size="2">
																Phone:</font></td>
																<td width="7">&nbsp;</td>
																<td>
																<input type="text" name="Phone_Number" size="20" class="txtbox"></td>
															</tr>
															<tr>
																<td align="right" width="97" height="40" valign="top">
																<font size="2">
																Message:</font></td>
																<td width="7">&nbsp;</td>
																<td>
																<textarea rows="5" name="Message" cols="14" class="txtbox"></textarea></td>
															</tr>
															<tr>
																<td align="right" width="97" height="7">
																</td>
																<td width="7" height="7"></td>
																<td height="7"></td>
															</tr>
															<tr>
																<td align="right" width="97" height="40">
																<font size="2">
																Security Code:</font></td>
																<td width="7" height="40">&nbsp;</td>
																<td height="40"><img src="securimage_show.php"><br>
																<input type="text" name="code" size="20" class="txtbox"></td>
															</tr>
															<tr>
																<td align="right" width="97">&nbsp;</td>
																<td width="7">&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td align="center" colspan="3">
																<input border="0" src="images/btnSend.jpg" name="I1" width="108" height="31" type="image"></td>
															</tr>
															<tr>
																<td align="right" width="97">&nbsp;</td>
																<td width="7">&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
														</table>
													</div>
												</td>
												<td width="10">&nbsp;</td>
												<td width="4" background="images/fpRight.jpg" style="background-image: url('images/fpRight.jpg'); background-repeat: repeat-y; background-position-x: right">&nbsp;</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
								<tr>
									<td height="8">
									<img border="0" src="images/fpBottom.jpg" width="273" height="8"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
						</div>
						</td>
						<td width="6">&nbsp;</td>
					</tr>
					<tr>
						<td width="80">&nbsp;</td>
						<td width="586">&nbsp;</td>
						<td width="37">&nbsp;</td>
						<td width="273">&nbsp;</td>
						<td width="6">&nbsp;</td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td align="left" height="167">
			<img border="0" src="images/bottomLeft.jpg" width="798" height="169"></td>
		</tr>
		<tr>
			<td bgcolor="#2D221C" height="157" align="left">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" height="157">
					<tr>
						<td align="left" width="22%">
			<img border="0" src="images/bottombottom.jpg" width="34" height="157"></td>
						<td width="62%"><div align="center">
						  <p><a href="index.php">Home</a> <b>
						  <font color="#F6EFDF">|</font></b>&nbsp;
						  <a href="property-listings.html">Property Listings</a>&nbsp; 
						  <b><font color="#F6EFDF">|</font></b>&nbsp;
						  <a href="the-community.html">The Community</a>&nbsp; <b>
						  <font color="#F6EFDF">|</font></b>&nbsp;
						  <a href="gallery.php">Gallery</a>&nbsp; <b>
						  <font color="#F6EFDF">|</font></b>&nbsp;
						  <a href="contact-us.html">Contact Us</a></p>
						  <p>&nbsp;</p>
						</div></td>
						<td width="16%"><p>&nbsp;</p>
					    <p>&nbsp;</p>
					    <p>&nbsp;</p>
					    <p><img src="images/Page1orbust_Logo_expose.jpg" width="187" height="30"> </p></td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
	</table>
</div>
</body>
</html>