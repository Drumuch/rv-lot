<?
session_start();
?><html>
<head>
<meta http-equiv="Content-Language" content="en-us">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>RV Lots</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="./css/form.css"/>
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    <script src="sweetalert-master/dist/sweetalert-dev.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>
<body>
<header>
    <div class="head_name_wrap">
        <div class="head_name">
            <span>RV-LOTS</span>
            <div class="head_menu_button fa fa-bars"></div>
        </div>
        <div class="head_menu_onclick">
            <a href="index.php">Home</a>
            <a href="property-listings.php">Property Listing</a>
            <a href="the-community.php">The Community</a>
            <a href="gallery.php">Gallery</a>
            <a href="contact-us.php">Contact Us</a>
        </div>
    </div>
    <?php include 'head.php' ; ?><!--****************HEAD****************-->
</header>
<main class="main">
    <div class="main_content">
        <div class="leftSideBar">
            <div class="content_header content_font">
                <p class="content_headertext1">RV lots For Sale</p>
            </div>
            <div class="content_main">
                <div class="content_main_textBar">
                    <img class="img_home" src="images/home_photo_2.jpg" alt=""/>
                    <span class="textBar_link">Talking Rock RV Resort</span>, located just west of Ellijay GA, offers the opportunity to own your very own <a href="property-listings.html">deeded RV lot</a>  in the beautiful mountain foothills of north Georgia. Only 5 minutes from 3,500 acre <a href="http://carters.sam.usace.army.mil/" target="_blank">Carters Lake </a> , considered by many to be Georgia's most spectacular and serene mountain lake. Well stocked with striped bass, hybrids and walleye, it is a fisherman's paradise.
                </div>
                <div class="content_main_textBar">
                    <img class="img_home2" src="images/home_photo_3.jpg" alt=""/>
                    With this comes a full amenity package including an all new clubhouse. Pool, tennis, pavilion, walking trails with excercise stations, excercise room, shower and laundry facilities, and modern playground equipment are just a few of the many amenities that make Talking Rock RV Resort stand out.
                </div>
                <div class="content_main_textBar">
                    <img class="img_home" src="images/home_photo.jpg" alt=""/>
                    The perfect affordable alternative to expensive mountain homes and cabins,<a href="the-community.php">Talking Rock RV Resort</a> offers you your very own<a href="property-listings.html">deeded RV site</a>, always ready for your getaways and seasonal stays. Leave your RV year-round if you choose.
                </div>
            </div>
        </div>
        <div class="rightSideBar ">
            <div class="rightSideBar_wrapper helpHeight">
                <div class="helpWrapper ">
                    <div class="featured_property">
                        <?
                        require "manage/db_connection.php";
                        $dirThumb=str_replace("../","",$dirThumb);
                        $dirFull=str_replace("../","",$dirFull);
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1'  ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $i=0;
                            while($row=mysql_fetch_array($result))
                            {
                                $ids[$i]=$row['id'];
                                $i++;
                            }
                            $id=$ids[rand(0,(count($ids)-1))];
                        }
                        else
                        {
                            ?>
                            <div align="left"><br>
                                <table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-top: 0px solid #000000;border-left: 0px solid #000000;border-right: 0px solid #000000;border-bottom: 0px solid #000000;" bordercolor="#000000">
                                    <tr>
                                        <td align="center"><font color="#ff0000">Your search returned no results.</font></td>
                                    </tr>
                                </table>
                            </div>
                        <?
                        }
                        if($id)
                        {
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1' AND id='".$id."' ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $row=mysql_fetch_array($result);
                        }
                        ?>
                        <p class="featured_property_head">Featured Property</p>
                        <p class="featured_property_lot"><?=$row['Property_Name'];?></p>
                        <div class="featured_property_main">
                            <a class="mainAnchor" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>">
                                <img class="mainImage" src="<?
                                if($row['thumb'] AND file_exists($dirThumb.$row['thumb']))
                                {
                                    echo $dirThumb.$row['thumb'];
                                }
                                else
                                {
                                    echo $dirThumb."noThumb.jpg";
                                }
                                ?>" alt=""/>
                            </a>
                                <span class="mainID">Listing ID:
                                    <?=$row['id'];?>
                                    <br>
                                    <?=str_replace("For ","",$row['Status'])." Price";?>
                                    :
                                    <?
                                    if($row['Price']>0)
                                    {
                                        ?>
                                        $
                                        <?=number_format($row['Price'],2);?>
                                    <?
                                    }
                                    else
                                    {
                                        echo "Call";
                                    }
                                    ?>
                                </span>
                            <span class="description"><?=substr($row['Description'],0,35);?> ...</span>
                            <a class="viewDetails" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>"> <img src="images/btnMore.jpg" width="82" height="23" alt=""> </a>
                            <? } ?>
                        </div>
                    </div>
                    <div class="clickBar">
                        <a href="property-listings.html">Click Here to View More Propeties</a>
                    </div>
                    <div class="contactBar">
                        <p>Wanda Blalock Century 21 Best</p>
                        <p>Mountain Properties</p>
                        <p>(706) 253-3707</p>
                        <p>(770) 894-1059</p>
                    </div>
                </div>
                <div class="formContact">
                    <form class="form" method="POST" action="formProcessor.php">
                        <div class="formwrapper">
                        <input type="hidden" name="formName" value="quick-contact">
                        <p class="form_head">Quick Contact</p>
                        <div class="formName">
                            <label for="formName">Name:</label> <input id="formName" name="Full_Name" type="text"/>
                        </div>
                        <div class="formAddress">
                            <label for="formAddress">Email Address:</label> <input id="formAddress" name="Email_Address" type="text"/>
                        </div>
                        <div class="formPhone">
                            <label for="formPhone">Phone:</label> <input id="formPhone" name="Phone_Number" type="text"/>
                        </div>
                        <div class="formMessage">
                            <label class="labelArea" for="formMessage">Message:</label> <textarea id="formMessage" name="Message" rows="5" name="Message" cols="14" class="formArea"></textarea>
                        </div>
                        <div class="formSecurity">
                            <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                            <img class="securityImage" src="securimage_show.php">-->
<!--                            <input id="formSecurity" name="code" type="text"/>-->
<!--                            <label for="formSecurity">Security Code:</label>-->
                        </div>
                        <div class="button"><input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image"></div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include 'foot.php' ; ?><!--****************FOOTER****************-->
</body>
</html>