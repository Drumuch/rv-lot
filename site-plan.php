<html>
<head>
    <meta http-equiv="Content-Language" content="en-us">
    <title>RV Lots - Map of the RV sites.</title>
    <META name="description" content="Mountain Resort RV Lots. RV Lots for sale in Georgia." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        <!--
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }
        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        //-->
    </script>
    <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>
<body id="sitePlan">
<header>
    <div class="head_name_wrap">
        <div class="head_name">
            <span>North Georgia RV Lots</span>
            <div class="head_menu_button fa fa-bars"></div>
        </div>
        <div class="head_menu_onclick">
            <a href="index.php">Home</a>
            <a href="property-listings.php">Property Listing</a>
            <a href="the-community.php">The Community</a>
            <a href="gallery.php">Gallery</a>
            <a href="contact-us.php">Contact Us</a>
        </div>
    </div>
    <?php include 'head.php' ; ?><!--****************HEAD****************-->
</header>
<main class="main">
    <div class="main_content">
        <div class="leftSideBar">
            <div class="content_header content_font">
                <p class="content_headertext1">RV lots For Sale</p>
            </div>
            <div class="content_main">
                <div class="content_main_textBar">
                    <p class="siteMapHead">Lot status changes weekly.</p>
                    <p class="siteMapHead">Call Susan Moody at 706-635-7272 or 1-800-809-9982 for details about properties available.</p>
                    <div class="wrapperSiteMap">
                        <img class="siteMap" src="images/Site_Map2.jpg" alt=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="rightSideBar">
            <div class="rightSideBar_wrapper">
                <div class="helpWrapper">
                    <div class="clickBar">
                        <a href="property-listings.html">Click Here to View More Propeties</a>
                    </div>
                    <div class="contactBar">
                        <p>Susan Moody</p>
                        <p>Woodland Realty</p>
                        <p>706-635-7272</p>
                        <p>1-800-809-9982</p>
                    </div>
                </div>
                <div class="formContact">
                    <form class="form" method="POST" action="formProcessor.php">
                        <div class="formwrapper">
                        <input type="hidden" name="formName" value="quick-contact">
                        <p class="form_head">Quick Contact</p>
                        <div class="formName">
                            <label for="formName">Name:</label> <input id="formName" name="Full_Name" type="text"/>
                        </div>
                        <div class="formAddress">
                            <label for="formAddress">Email Address:</label> <input id="formAddress" name="Email_Address" type="text"/>
                        </div>
                        <div class="formPhone">
                            <label for="formPhone">Phone:</label> <input id="formPhone" name="Phone_Number" type="text"/>
                        </div>
                        <div class="formMessage">
                            <label class="labelArea" for="Message">Message:</label> <textarea id="formMessage" rows="5" name="Message" cols="14" class="formArea"></textarea>
                        </div>
                        <div class="formSecurity">
                            <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                            <img class="securityImage" src="securimage_show.php">-->
<!--                            <input id="formSecurity" name="code" type="text"/>-->
<!--                            <label for="formSecurity">Security Code:</label>-->
                        </div>
                        <div class="button"><input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image"></div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include 'foot.php' ; ?><!--****************FOOTER****************-->
</body>
</html>