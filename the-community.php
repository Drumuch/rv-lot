<!DOCTYPE html>
<html lang="en">
<head>
    <title>RV Lots for Sale - Deeded RV Lot in Resort Community.</title>
    <META name="description" content="Mountain Resort RV Lots. RV Lots for sale in Georgia." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    <script src="sweetalert-master/dist/sweetalert-dev.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body id="theCommunity">
    <header>
        <div class="head_name_wrap">
            <div class="head_name">
                <span>RV Resort Community</span>
                <div class="head_menu_button fa fa-bars"></div>
            </div>
            <div class="head_menu_onclick">
                <a href="index.php">Home</a>
                <a href="property-listings.php">Property Listing</a>
                <a href="the-community.php">The Community</a>
                <a href="gallery.php">Gallery</a>
                <a href="contact-us.php">Contact Us</a>
            </div>
        </div>
        <?php include 'head.php' ; ?><!--****************HEAD****************-->
    </header>
    <main class="main">
        <div class="main_content">
            <div class="leftSideBar">
                <div class="content_header content_font">
                    <p class="content_headertext1">The Community</p>
                    <p class="content_headertext2">What Talking Rock Can Offer You</p>
                    <p class="content_headertext3">Here at Talking Rock RV Resort, we tend to be a laid-back, enjoy life as it comes community. </p>
                </div>
                <div class="content_main">
                    <div class="content_main_textBar">
                        <img class="img_home" src="images/comm_1.jpg" alt=""/>
                        RV living is certainly an adventure, and it&rsquo;s one that so many people tend to miss out on. The nomadic fun, the ever changing surroundings, and the eclectic people you get to meet when you travel in an RV makes it dynamic and enjoyable living. If you think that this sort of lifestyle is going to suit you, Talking Rock RV Resort is the place to go. From the gorgeous scenery to the affordable prices, our <a href="index.php">RV lots</a> here in Ellijay, Georgia have something for everyone to enjoy. Browse around to discover all that we could offer you. <br>
                        Talking Rock Resort is such a great location to consider when you want to buy RV lots because it has all the amenities of a fine hotel right in the great outdoors. The views here are amazing as you get to have your lot right on the foothills of northern Georgia. You can also find a tennis court, a gym, a laundry facility, a playground and a pool within the resort, making it one of the most versatile places to live in an RV. With the RV lots for sale around here, you get more than just a plot of land to park your RV home. You get a full experience that only Talking Rock Resort has to offer.
                    </div>
                    <div class="content_main_textBar">
                        <img class="img_home2" src="images/comm_2.jpg" alt=""/>
                        In terms of the <a href="property-listings.php">RV sites for sale</a>, any of our lots are a mere 5 minutes away from the massive 3,500 acre Carters Lake, making this a great place to live if you enjoy fishing or water sports. The lake is full of walleye, striped bass, and hybrids that you can fish for, so don&rsquo;t forget to bring the bait along. With <a href="the-community.php">Talking Rock RV Resort</a>, we try to make sure that you aren&rsquo;t just going to buy RV lots to park at. We want you to be pulling up to a true vacation spot and home away from home. With our comfort and amenities, we promise you will find just that. <br>
                        Whether you want to stay for weekend retreats or you plan to enjoy all 4 seasons, Talking Rock RV Resort is a great choice. We are in the heart of some of Georgia&rsquo;s finest scenery, and we make sure that the beauty of the surroundings is only highlighted by our RV sites for sale. <br>
                        <br/>
                        <p class="style10 style38">We have no permanent residents. Everyone here is here for the fun of the RV lifestyle and the great enjoyment presented by the surroundings.</p>   </div>
                </div>
            </div>
            <div class="rightSideBar">
                <div class="rightSideBar_wrapper">
                    <div class="helpWrapper">
                        <div class="clickBar">
                            <a href="property-listings.html">Click Here to View More Propeties</a>
                        </div>
                        <div class="coosawattee">
                            <a href="http://www.ourlot.com/resortrvlots.php" target="_blank"><img src="images/RV-LOTS_coosawatee-lots.jpg" alt="RV LOTS in Coosawattee, GA" width="273" height="100" border="0"></a>
                        </div>
                        <div class="contactBar">
                            <p>Susan Moody</p>
                            <p>Woodland Realty</p>
                            <p>706-635-7272</p>
                            <p>1-800-809-9982</p>
                        </div>
                    </div>
                    <div class="formContact">
                        <form class="form" method="POST" action="formProcessor.php">
                            <div class="formwrapper">
                            <input type="hidden" name="formName" value="quick-contact">
                            <p class="form_head">Quick Contact</p>
                            <div class="formName">
                                <label for="formName">Name:</label> <input id="formName" name="Full_Name" type="text"/>
                            </div>
                            <div class="formAddress">
                                <label for="formAddress">Email Address:</label> <input id="formAddress" name="Email_Address" type="text"/>
                            </div>
                            <div class="formPhone">
                                <label for="formPhone">Phone:</label> <input id="formPhone" name="Phone_Number" type="text"/>
                            </div>
                            <div class="formMessage">
                                <label class="labelArea" for="formMessage">Message:</label> <textarea id="formMessage" name="Message" rows="5" name="Message" cols="14" class="formArea"></textarea>
                            </div>
                            <div class="formSecurity">
                                <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                                <img class="securityImage" src="securimage_show.php">-->
<!--                                <input id="formSecurity" name="code" type="text"/>-->
<!--                                <label for="formSecurity">Security Code:</label>-->
                            </div>
                            <div class="button"><input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image"></div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'foot.php' ; ?><!--****************FOOTER****************-->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4494118-26']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>