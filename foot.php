<footer>
    <div class="foot_image_car"></div>
    <div class="foot_bottom">
        <div class="foot_wrapperLink">
            <a class="foot_home" href="index.php">Home</a>
            <a class="foot_listing" href="property-listings.php">Property Listings</a>
            <a class="foot_living" href="livingrv.php">RV Living</a>
            <a class="foot_community" href="the-community.php">The Community</a>
            <a class="foot_gallery" href="gallery.php">Gallery</a>
            <a class="foot_map" href="site-plan.php">Site Map</a>
            <a class="foot_direction" href="directions-and-map.php">Directions</a>
            <a class="foot_contact" href="contact-us.php">Contact Us</a>
        </div>
        <div class="foot_copyright">Copyright ©  Flint Timber LP</div>
        <div class="foot_info">
            <div class="foot_infotext1">Web Design and SEO</div>
            <a href="http://www.pageoneorbust.com" class="foot_infotext2">Page One or Bust</a>
        </div>
    </div>
</footer>