<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
    <title>RV LOTS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    <script src="sweetalert-master/dist/sweetalert-dev.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <div class="head_name_wrap">
        <div class="head_name">
            <span>North Georgia RV Lots</span>
            <div class="head_menu_button fa fa-bars"></div>
        </div>
        <div class="head_menu_onclick">
            <a href="index.php">Home</a>
            <a href="property-listings.php">Property Listing</a>
            <a href="the-community.php">The Community</a>
            <a href="gallery.php">Gallery</a>
            <a href="contact-us.php">Contact Us</a>
        </div>
    </div>
    <?php include 'head.php' ; ?><!--****************HEAD****************-->
</header>
<main class="main">
    <div class="main_content avtoH">
        <p class="thank">
            Thank you for contacting us.
            <br/>
            <br/>
            We will get back to you as soon as possible to assist with your enquiry.
        </p>
    </div>
</main>
<?php include 'foot.php' ; ?><!--****************FOOTER****************-->
<script language="javascript">
swal("Thank you! Your message has been sent.");
</script>
</body>
</html>