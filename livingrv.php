<html>
<head>

    <title>Resort RV Living. Lots available for Sale in GA mountains.</title>
<meta http-equiv="Content-Language" content="en-us">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/main.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<header>
    <div class="head_name_wrap">
        <div class="head_name">
            <span>North Georgia RV Lots</span>
            <div class="head_menu_button fa fa-bars"></div>
        </div>
        <div class="head_menu_onclick">
            <a href="index.php">Home</a>
            <a href="property-listings.php">Property Listing</a>
            <a href="the-community.php">The Community</a>
            <a href="gallery.php">Gallery</a>
            <a href="contact-us.php">Contact Us</a>
        </div>
    </div>
    <?php include 'head.php' ; ?><!--****************HEAD****************-->
</header>
<main class="main">
    <div class="main_content">
        <div class="leftSideBar">
            <div class="content_header content_font">
                <p class="content_headertext1">Living RV Life</p>
                <p class="content_headertext2">A Home Away From Home</p>
            </div>
            <div class="content_main">
                <div class="content_main_textBar">
                    <p>If you've ever wondered what RV living was like, you might be surprised to find out how similar it is to everyday living. An RV is nothing more than a condensed house on wheels. Most models feature all the amenities from home that you could want, but they just squeeze down on the space to make everything portable. You can still cook, sleep, shower and entertain in an RV. You just have to adjust to having smaller square footage to deal with. You can find a great home away from home with RV resorts if you decide to take the chance to do so.</p>
                    <br/>
                    <img class="img_home" src="images/livervlife.jpg" alt=""/>
                    The style of living in RV resorts is perfect for vacations or retired individuals who are simply ready for a getaway. Women in particular often love living in an RV because that equates to less surfaces that have to be cleaned. While a three bedroom home in the suburbs may be charming, it can also be a pain to keep clean. The simplicity of RV living makes things a whole lot easier. The only sacrifice with space is a lack of storage, but that's just a matter of using less to do more.
                    <br/>
                    <br/>
                    RV resorts are great communities to live in because you never know who you are going to meet. Your neighbor to the left may be a retired war veteran, and the neighbor on the right could be a former singing sensation. There is such an eclectic mix of people found within RV parks that it’s almost impossible not to enjoy something about this style of living. If you like meeting new people or enjoy learning about other ways of life, RV resorts will be perfect for you. Everyone there is looking for a pleasant escape from the world, and it is that common drive that brings the whole community together.
                    <br/>
                    <br/>
                    RV park living is worth a shot if you feel like something new. You might be able to get involved with community pot lucks or even the occasional bingo tournament, depending on the type of people living around you. If all you want to do is watch satellite TV at home, you can do that too. This style of living provides for maximum flexibility, so you can do whatever you want to do whenever you want to do it. If you are ready to just get away from it all, get a spot in an <a href="index.php">RV lot</a> today.
                </div>
            </div>
        </div>
        <div class="rightSideBar">
            <div class="rightSideBar_wrapper">
                <div class="helpWrapper">
                    <div class="featured_property">
                        <?
                        require "manage/db_connection.php";
                        $dirThumb=str_replace("../","",$dirThumb);
                        $dirFull=str_replace("../","",$dirFull);
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1'  ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $i=0;
                            while($row=mysql_fetch_array($result))
                            {
                                $ids[$i]=$row['id'];
                                $i++;
                            }
                            $id=$ids[rand(0,(count($ids)-1))];
                        }
                        else
                        {
                            ?>
                            <div align="left"><br>
                                <table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-top: 0px solid #000000;border-left: 0px solid #000000;border-right: 0px solid #000000;border-bottom: 0px solid #000000;" bordercolor="#000000">
                                    <tr>
                                        <td align="center"><font color="#ff0000">Your search returned no results.</font></td>
                                    </tr>
                                </table>
                            </div>
                        <?
                        }
                        if($id)
                        {
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1' AND id='".$id."' ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $row=mysql_fetch_array($result);
                        }
                        ?>
                        <p class="featured_property_head">Featured Property</p>
                        <p class="featured_property_lot"><?=$row['Property_Name'];?></p>
                        <div class="featured_property_main">
                            <a class="mainAnchor" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>">
                                <img class="mainImage" src="<?
                                if($row['thumb'] AND file_exists($dirThumb.$row['thumb']))
                                {
                                    echo $dirThumb.$row['thumb'];
                                }
                                else
                                {
                                    echo $dirThumb."noThumb.jpg";
                                }
                                ?>" alt=""/>
                            </a>
                                <span class="mainID">Listing ID:
                                    <?=$row['id'];?>
                                    <br>
                                    <?=str_replace("For ","",$row['Status'])." Price";?>
                                    :
                                    <?
                                    if($row['Price']>0)
                                    {
                                        ?>
                                        $
                                        <?=number_format($row['Price'],2);?>
                                    <?
                                    }
                                    else
                                    {
                                        echo "Call";
                                    }
                                    ?>
                                </span>
                            <span class="description"><?=substr($row['Description'],0,35);?> ...</span>
                            <a class="viewDetails" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>"> <img src="images/btnMore.jpg" width="82" height="23" alt=""> </a>
                            <? } ?>
                        </div>
                    </div>
                    <div class="clickBar">
                        <a href="property-listings.html">Click Here to View More Propeties</a>
                    </div>
                    <div class="contactBar">
                        <p>Wanda Blalock Century 21 Best</p>
                        <p>Mountain Properties</p>
                        <p>(706) 253-3707</p>
                        <p>(770) 894-1059</p>
                    </div>
                </div>
                <div class="formContact">
                    <form class="form" method="POST" action="formProcessor.php">
                        <div class="formwrapper">
                        <input type="hidden" name="formName" value="quick-contact">
                        <p class="form_head">Quick Contact</p>
                        <div class="formName">
                            <label for="formName">Name:</label> <input id="formName" name="Full_Name" type="text"/>
                        </div>
                        <div class="formAddress">
                            <label for="formAddress">Email Address:</label> <input id="formAddress" name="Email_Address" type="text"/>
                        </div>
                        <div class="formPhone">
                            <label for="formPhone">Phone:</label> <input id="formPhone" name="Phone_Number" type="text"/>
                        </div>
                        <div class="formMessage">
                            <label class="labelArea" for="Message">Message:</label> <textarea id="formMessage" rows="5" name="Message" cols="14" class="formArea"></textarea>
                        </div>
                        <div class="formSecurity">
                            <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                            <img class="securityImage" src="securimage_show.php">-->
<!--                            <input id="formSecurity" name="code" type="text"/>-->
<!--                            <label for="formSecurity">Security Code:</label>-->
                        </div>
                        <div class="button"><input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image"></div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include 'foot.php' ; ?><!--****************FOOTER****************-->
</body>
</html>