$(document).ready(function(){
    $('body').bind("DOMSubtreeModified",function(){
        if ($(this).children().length === 5) {
                var a = $(this).children().eq(3);
            a.addClass('ggg');
        }
    });
    // CLICK ON MENU BUTTON *************************************************************
    $('.head_menu_button').click(function() {
        if ( $( '.head_menu_button' ).hasClass("active") ) {
            $(".head_menu_onclick").removeClass('menu_active');
            $('.head_menu_button').removeClass("active");
            console.log("remove");
        } else {
            $(".head_menu_onclick").addClass('menu_active');
            $('.head_menu_button').addClass("active");
            console.log("add");
        }
    });
    // CLICK ON MENU BUTTON *************************************************************
    $('.fixedHead_side_bar').click(function() {
        if ($(this).hasClass('activeB')) {
            $(this).css({
                left: "-245px",
                height: "100px"
            })
                .removeClass('activeB');
            $('.close_button').css({
                top: "42px",
                left: "0px"
            })
                .removeClass('fa-times')
                .addClass('fa-arrow-right');
        } else {
            $(this).css({
                left: "0px",
                height: "183px"
            })
                .addClass('activeB');
            $('.close_button').css({
                top: "0px",
                left: "-10px"
            })
                .removeClass('fa-arrow-right')
                .addClass('fa-times');
        }
    });
    (window.onresize = function(){
      if ($('.head_name_wrap').width() > 760) {
          $(".head_menu_onclick").removeClass('menu_active');
      }

    })();
    //function pos() {
    //    var windowW = document.documentElement.clientWidth,
    //        windowH = document.documentElement.clientHeight,
    //        sweetAl = document.querySelector(".sweet-alert"),
    //        alertW = sweetAl.offsetWidth,
    //        alertH = sweetAl.offsetHeight;
    //    console.log(sweetAl + "\n alertwidth: " + alertW + "\n alertheight: " + alertH + "\n clientwidth: " + windowW + "\n clientheight: " + windowH );
    //    sweetAl.style.top = (windowH - alertH)/2 + "px";
    //    sweetAl.style.left = (windowW - alertW)/2 + "px";
    //    sweetAl.style.marginLeft = 0 + "px";
    //    sweetAl.style.marginTop = 0 + "px";
    //}
    //pos();
    function mySweetAlert(){
        var $w = $(window).width();
        var $h = document.body.clientHeight;
        var $saw = $('.sweet-alert').innerWidth();
        var $sah = $('.sweet-alert').innerHeight();
        var $leftPos = ($w - $saw)/2;
        var $topPos = ($h - $sah)/2;
        var sweetAl = document.querySelector(".sweet-alert");
        $('.sweet-alert').css({"top": $topPos+"px", "left": $leftPos+'px' });
        sweetAl.style.marginLeft = 0 + "px";
        sweetAl.style.marginTop = 0 + "px";
        console.log($w+'  '+$h+"\n"+$saw+' '+$sah+"\n"+$leftPos+' '+$topPos+"\n"+$('.sweet-alert'));
    };
    mySweetAlert();
});

