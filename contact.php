<?
session_start();
?><html>
<head>
    <meta http-equiv="Content-Language" content="en-us">
    <title>RV Lots - Contact Us To Request More Information About Resort RV Properties.</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="./css/form.css"/>
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/jquery.bxslider.css"/>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    <script src="sweetalert-master/dist/sweetalert-dev.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/main.js"></script>
    <script src="js/gallery.js"></script>
    <script type="text/javascript">

        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }
        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
    </script>
    <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>
<body>
<header>
    <div class="head_name_wrap">
        <div class="head_name">
            <span>North Georgia RV Lots</span>
            <div class="head_menu_button fa fa-bars"></div>
        </div>
        <div class="head_menu_onclick">
            <a href="index.php">Home</a>
            <a href="property-listings.php">Property Listing</a>
            <a href="the-community.php">The Community</a>
            <a href="gallery.php">Gallery</a>
            <a href="contact-us.php">Contact Us</a>
        </div>
    </div>
    <?php include 'head.php' ; ?><!--****************HEAD****************-->
</header>
<main class="main">
    <div class="main_content">
        <div class="leftSideBar">
            <div class="content_header content_font">
                <p class="content_headertext1 MarginL">Contact Us</p>
            </div>
            <div class="content_main">
                <div class="contact_formBox">
                    <form method="POST" action="formProcessor.php">
                        <input type="hidden" name="formName" value="contact">

                        <div class="contact_form_name">
                            <label for="Full_Name">FullName:</label> <input class="contact_input decoration_boxes" id="contactFormName" name="Full_Name" type="text"/>
                        </div>
                        <div class="contact_form_email">
                            <label for="Email_Address">Email Address:</label> <input class="contact_input decoration_boxes" id="contactFormAddress" name="Email_Address" type="text"/>
                        </div>
                        <div class="contact_form_address">
                            <label for="Message0">Address:</label> <textarea class="contact_input decoration_boxes" rows="2" name="Message0" cols="14" class="contact_form_message0"></textarea>
                        </div>
                        <div class="contact_form_city">
                            <label for="Email_Address0">City/State/Zip:</label> <input type="text" name="Email_Address0" class="address0 decoration_boxes"> / <input type="text" name="Email_Address1" class="address1 decoration_boxes"> / <input type="text" name="Email_Address2" class="address2 decoration_boxes">
                        </div>
                        <div class="contact_form_city460">
                            <div class="contactCity">
                                <label for="Email_Address0">City:</label>
                                <input type="text" name="Email_Address0" class="address0 decoration_boxes">
                            </div>
                            <div class="contactState">
                                <label for="Email_Address0">State:</label>
                                <input type="text" name="Email_Address1" class="address1 decoration_boxes">
                            </div>
                            <div class="contactZip">
                                <label for="Email_Address0">Zip:</label>
                                <input type="text" name="Email_Address2" class="address2 decoration_boxes">
                            </div>
                        </div>
                        <div class="contact_form_phone">
                            <label for="Phone_Number">Phone Number:</label> <input class="contact_input decoration_boxes" id="contactFormPhone" name="Phone_Number" type="text"/>
                        </div>
                        <div class="contact_form_message">
                            <label for="Message">Message:</label> <textarea class="contact_input decoration_boxes" rows="5" name="Message" cols="14" class="contact_form_message"></textarea>
                        </div>
                        <div class="contact_form_code">
                            <div class="g-recaptcha" data-sitekey="6LdNaQ8TAAAAANuIyiewLP1aaFgVi1pnTj2XvIRs"></div>
<!--                            <input class="contactFormS"  name="code" type="text"/>-->
<!--                            <img class="securityContactImage" src="securimage_show.php">-->
                        </div>
                        <div class="contactButton">
                            <input class="formButton" src="images/btnSend.jpg" name="sBtn" width="108" height="31" type="image">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="rightSideBar qc">
            <div class="rightSideBar_wrapper">
                <div class="helpWrapper qc">
                    <div class="featured_property">
                        <?
                        require "manage/db_connection.php";
                        $dirThumb=str_replace("../","",$dirThumb);
                        $dirFull=str_replace("../","",$dirFull);
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1'  ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $i=0;
                            while($row=mysql_fetch_array($result))
                            {
                                $ids[$i]=$row['id'];
                                $i++;
                            }
                            $id=$ids[rand(0,(count($ids)-1))];
                        }
                        else
                        {
                            ?>
                            <div align="left"><br>
                                <table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-top: 0px solid #000000;border-left: 0px solid #000000;border-right: 0px solid #000000;border-bottom: 0px solid #000000;" bordercolor="#000000">
                                    <tr>
                                        <td align="center"><font color="#ff0000">Your search returned no results.</font></td>
                                    </tr>
                                </table>
                            </div>
                        <?
                        }
                        if($id)
                        {
                        $sql="SELECT * FROM ".$prefix."_properties WHERE state='1' AND id='".$id."' ORDER BY id DESC LIMIT 0 , 15";
                        $result=mysql_query($sql) or die("Extracting Properties Data Failed!<br>Permanent Error: " . mysql_error());
                        if($result AND mysql_num_rows($result)>0)
                        {
                            $row=mysql_fetch_array($result);
                        }
                        ?>
                        <p class="featured_property_head">Featured Property</p>
                        <p class="featured_property_lot"><?=$row['Property_Name'];?></p>
                        <div class="featured_property_main">
                            <a class="mainAnchor" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>">
                                <img class="mainImage" src="<?
                                if($row['thumb'] AND file_exists($dirThumb.$row['thumb']))
                                {
                                    echo $dirThumb.$row['thumb'];
                                }
                                else
                                {
                                    echo $dirThumb."noThumb.jpg";
                                }
                                ?>" alt=""/>
                            </a>
                                <span class="mainID">Listing ID:
                                    <?=$row['id'];?>
                                    <br>
                                    <?=str_replace("For ","",$row['Status'])." Price";?>
                                    :
                                    <?
                                    if($row['Price']>0)
                                    {
                                        ?>
                                        $
                                        <?=number_format($row['Price'],2);?>
                                    <?
                                    }
                                    else
                                    {
                                        echo "Call";
                                    }
                                    ?>
                                </span>
                            <span class="description"><?=substr($row['Description'],0,35);?> ...</span>
                            <a class="viewDetails" href="property-listings.html?id=<?=$row['id'];?>&Status=<?=$Status;?>&Type=<?=$Type;?>&Price=<?=$Price;?>&Location_State=<?=$Location_State;?>&Location_City=<?=$Location_City;?>"> <img src="images/btnMore.jpg" width="82" height="23" alt=""> </a>
                            <? } ?>
                        </div>
                    </div>
                    <div class="clickBar">
                        <a href="property-listings.html">Click Here to View More Propeties</a>
                    </div>
                    <div class="contactBar">
                        <p>Susan Moody</p>
                        <p>Woodland Realty</p>
                        <p>706-635-7272</p>
                        <p>1-800-809-9982</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include 'foot.php' ; ?><!--****************FOOTER****************-->
</body>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-4494118-26']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</html>